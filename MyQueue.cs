using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson23_HW
{
    class MyQueue
    {
        private List<Customer> customers;

        public MyQueue()
        {
            customers = new List<Customer>();
        }

        public void Enqueue(Customer newCustomer)
        {
            this.customers.Add(newCustomer);
        }

        public Customer Dequeue()
        {
            if (customers.Count > 0)
            {
                Customer value = this.customers[0];
                this.customers.RemoveAt(0);
                return value;
            }
            return null;
        }

        public void Init(List<Customer> newCustomers)
        {
            this.customers = newCustomers;
        }

        public void Clear()
        {
            this.customers.Clear();
        }

        public Customer WhoIsNext()
        {
            if (customers.Count > 0)
            {
                Customer value = this.customers[0];
                return value;
            }
            return null;
        }

        public int Count()
        {
            return this.customers.Count();
        }

        public void SortByProtection()
        {
            this.customers.Sort(new SortClassByProtection());
        }

        public void SortByTotalPurchases()
        {
            this.customers.Sort(new SortClassByTotalPurchases());

        }

        public void SortByBirthYear()
        {
            this.customers.Sort();
        }

        public List<Customer> DequeueCustomer(int x)
        {
            if (customers.Count == 0)
            {
                return new List<Customer>();
            }
            List<Customer> list = new List<Customer>();

            while (customers.Count > 0 && list.Count < x)
            {
                list.Add(customers[0]);
                customers.RemoveAt(0);
            }
            return list;
        }

        public void AniRakSheela(Customer newCustomer)
        {
            this.customers.Insert(0, newCustomer);
        }

         public Customer DequeueProtectiza()
        {
            if (this.customers.Count > 0)
            {
                //int maxProtection = customers[0].Protection;
                //int index = 0;

                //for (int i = 1; i < customers.Count; i++)
                //{
                //    Customer c = customers[i];
                //    if (c.Protection > maxProtection)
                //    {
                //        maxProtection = c.Protection;
                //        index = i;
                //    }
                //}

                //Customer result = customers[index];
                //customers.RemoveAt(index);
                //return result;

                //LINQ Answer
                int max = customers.Max(x => x.Protection);
                int maxIndex = customers.FindIndex(m => m.Protection == max);
                Customer result = customers[maxIndex];
                return result;
            }
            return null;
    }

   
}
