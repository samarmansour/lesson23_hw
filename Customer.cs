﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson23_HW
{
    class Customer: IComparable<Customer>
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int BirthYear { get; set; }
        public string Address { get; set; }
        public int Protection { get; set; }
        public int TotalPurchases { get; set; }

        public Customer(int id, string name, int birthYear, string address, int protection)
        {
            ID = id;
            Name = name;
            BirthYear = birthYear;
            Address = address;
            Protection = protection;
        }

        public override string ToString()
        {
            return $"ID: {this.ID}  Name: {this.Name}  Birth year: {this.BirthYear}  Address: {this.Address}  Protection: {this.Protection}  Total Purchases: {this.TotalPurchases}";
        }

        public int CompareTo(Customer other)
        {
            return (other.BirthYear - this.BirthYear) * -1;
        }
    }

    class SortClassByProtection : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return x.Protection - y.Protection;
        }
    }

    class SortClassByTotalPurchases : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return x.TotalPurchases - y.TotalPurchases;
        }
    }
}
