using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson23_HW
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Customer> customerList = new List<Customer>
            {
                new Customer(1,"Naya",1990,"La mesa 30, LA", 5),
                new Customer(2,"Jack",1980,"Par Drive 2465, NY", 15),
                new Customer(3,"Ralf",2013,"Pin Oak Drive 2547, CA", 10),
                new Customer(4,"Eva",1991,"Armbrester Drive 2714, CA", 15)
            };
            customerList.ForEach(t => Console.WriteLine(t));
            Console.WriteLine();


            MyQueue customers = new MyQueue();
            Console.WriteLine( customers.Count() + " Length");

            customers.Init(customerList);
            Console.WriteLine(customers.Count() + " Length");

            customers.Enqueue(new Customer(1, "Naya", 1990, "La mesa 30, LA", 5));
            Console.WriteLine(customers.Count() + " Length");

            Console.WriteLine($"{ customers.Dequeue()} has been Removed");
            Console.WriteLine( customers.WhoIsNext() + " is next");
            
            Console.WriteLine("=============Sort By Protection===============");
            customers.SortByProtection();
            customerList.ForEach(t => Console.WriteLine(t));

            Console.WriteLine("=============Sort By Total===============");
            customers.SortByTotalPurchases();
            customerList.ForEach(t => Console.WriteLine(t));


            Console.WriteLine("=============Sort By Year===============");
            customers.SortByBirthYear();
            customerList.ForEach(t => Console.WriteLine(t));

            Console.WriteLine();
            customers.DequeueCustomer(2);
            customerList.ForEach(t => Console.WriteLine(t));

            Console.WriteLine();
            Console.WriteLine($"{customers.DequeueProtectiza()} max Protection");

            Console.WriteLine();
            customers.AniRakSheela(new Customer(5, "Naya", 1985, "La mesa 30, LA", 20));
            Console.WriteLine($"{customers.DequeueProtectiza()} max Protection");




        }


    }
}
